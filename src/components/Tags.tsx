/* Instruments */
import { icons } from "../theme/icons/tag";

const NodeJSIcon = icons["Node.js"]
const NextJSIcon = icons["Next.js"]
const UIUXIcon = icons["UI/UX"]

export const Tags = () => {
  
  return (
    <div className="tags">
      <span>
        <icons.React /> React
      </span>
      <span>
        <NodeJSIcon /> Node.js
      </span>
      <span>
        <icons.JavaScript /> JavaScript
      </span>
      <span>
        <icons.TypeScript /> TypeScript
      </span>
      <span>
        <NextJSIcon /> Next.js
      </span>
      <span>
        <icons.VSCode /> VSCode
      </span>
      <span>
        <icons.CSS /> CSS
      </span>
      <span>
        <icons.Git /> Git
      </span>
      <span>
        <icons.GraphQL /> GraphQL
      </span>
      <span>
        <UIUXIcon /> UI/UX
      </span>
      <span>
        <icons.Npm /> Npm
      </span>
      <span>
        <icons.Testing /> Testing
      </span>
      <span>
        <icons.macOS /> macOS
      </span>
      <span>
        <icons.Tools /> Tools
      </span>
    </div>
  );
};
