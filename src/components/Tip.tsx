/* Instruments */
import { icons } from "../theme/icons/tag"


export const Tip = () => {
  return (
    <article>
      <header>
        <icons.JavaScript /> 
        <h1>Пользуйся правильными переменными</h1>
      </header>
      <main>
        <time>
          <icons.JavaScript /> 
          <div>
            <span>🚀 20.03.2021, 13:35</span>
            <span>Автор: Lectrum</span>
          </div>
        </time>
        <h2>Пользуйся правильными переменными</h2>
        <p>Не var а const и let</p>
      </main>
      <footer>
        <a href="/topic-by-tag/cb19d422-fc39-41cb-b775-a93915f2d3c1">📖  Читать полностью →</a>
      </footer>
    </article>
  )
}
