/* Components */
import { Nav } from '../components/Nav';
import { Tips } from "../components/Tips";
import { Tags } from "../components/Tags"

/* Instruments */
import { icons } from '../theme/icons/tag';

export const HomePage = () => {
    return (
        <section className="layout">
            <section className="hero">
                <div className="title">
                    <h1>Типсы и Триксы</h1>
                    <h2>React</h2>
                </div>
                <Tags />

            </section>
            <Nav />
            <Tips />
        </section>
    );
};
