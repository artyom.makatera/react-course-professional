/* Components */
import { icons } from '../theme/icons/nav';

export const Nav = () => {
    return (
        <nav className="nav">
            <h1>Т и T</h1>
            <a>
                <icons.Home /> Все темы
            </a>
            <a>
                <icons.Tag /> По тэгам
            </a>
            <a>
                <icons.Publish /> Опубликовать
            </a>
            <a>
                <icons.Search /> Поиск
            </a>
        </nav>
    );
};
