/* Components */
import { Tip } from "./Tip";

export const Tips = () => {
  return (
    <section className="tip-list">
      <Tip />
    </section>
  )
}
